// Middleware for decoding token
const jwt = require("jsonwebtoken")

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config()
}

module.exports = function(req, res, next) {
  const token = req.header("x-auth-token")

  if (!token) {
    return res.status(401).json({ msg: " No token" })
  }

  try {
    const decoded = jwt.verify(token, process.env.SECRET)

    req.user = { id: decoded.id, email: decoded.email }
    next()
  } catch (err) {
    res.status(401).json({ msg: "Token is not valid" })
  }
}
