const http = require("http")
const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")
const app = express()
const hellorouter = require("./routes/hellorouter")
//const userRouter = require('./routes/user')

// This file contains express setup. Read more about this here: https://expressjs.com/
// Init Middleware
// Cors enables cross-origin requests. Read more about this here: https://github.com/expressjs/cors#readme
app.use(cors())
app.use(bodyParser.json())
//app.use(tokenExtractor.tokenExtractor)

// Define Routes
app.use("/helloworld", hellorouter)
app.use("/api/tickets", require("./routes/tickets"))
app.use("/api/users", require("./routes/users"))
app.use("/api/products", require("./routes/products"))

const server = http.createServer(app)

const PORT = 5000
//const HOST = '0.0.0.0'

server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})

//curl --header "Content-Type: application/json" -d "{\"email\":\"test@testi.fi\",\"password\":\"passu\"}" http://localhost:5000/api/user/register
// curl --get "localhost:5000/helloworld"
