const mysql = require("mysql2/promise")
// More information https://www.npmjs.com/package/mysql2
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config()
}
// The process.env variables come from .env file
// In production environment, the .env should not be
// committed to Git, ignore it or use system's env variables instead
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  multipleStatements: true
})

module.exports = pool
