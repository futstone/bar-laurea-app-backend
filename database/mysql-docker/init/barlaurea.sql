-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema barlaurea_dev1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema barlaurea_dev1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `barlaurea_dev1` DEFAULT CHARACTER SET utf8 ;
USE `barlaurea_dev1` ;

-- -----------------------------------------------------
-- Table `barlaurea_dev1`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barlaurea_dev1`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barlaurea_dev1`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barlaurea_dev1`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `passhash` VARCHAR(60) NOT NULL,
  `role` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_1_idx` (`role` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  CONSTRAINT `fk_user_1`
    FOREIGN KEY (`role`)
    REFERENCES `barlaurea_dev1`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barlaurea_dev1`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barlaurea_dev1`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barlaurea_dev1`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barlaurea_dev1`.`ticket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `id_product` INT NOT NULL,
  `transaction_id` VARCHAR(45) NOT NULL,
  `order_id` VARCHAR(45) NOT NULL,
  `status` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ticket_1_idx` (`id_user` ASC),
  INDEX `fk_ticket_2_idx` (`id_product` ASC),
  CONSTRAINT `fk_ticket_1`
    FOREIGN KEY (`id_user`)
    REFERENCES `barlaurea_dev1`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_2`
    FOREIGN KEY (`id_product`)
    REFERENCES `barlaurea_dev1`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `barlaurea_dev1`.`price`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `barlaurea_dev1`.`price` (
  `id_role` INT NOT NULL,
  `id_product` INT NOT NULL,
  `amount` INT NOT NULL,
  PRIMARY KEY (`id_role`, `id_product`),
  INDEX `fk_price_2_idx` (`id_product` ASC),
  CONSTRAINT `fk_price_1`
    FOREIGN KEY (`id_role`)
    REFERENCES `barlaurea_dev1`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_price_2`
    FOREIGN KEY (`id_product`)
    REFERENCES `barlaurea_dev1`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- INSERT INITIAL DATA
-- -----------------------------------------------------
INSERT INTO role (name) VALUES ('guest'), ('student'), ('staff');

INSERT INTO product (name) VALUES ('Lounas'), ('Kahvi');

INSERT INTO price (id_role, id_product, amount) VALUES 
(1, 1, 680),
(1, 2, 220),
(2, 1, 280),
(2, 2, 150),
(3, 1, 480),
(3, 2, 180);

INSERT INTO user (email, passhash, role) VALUES ('Testii', '$2a$10$HUX8uZQ4PlzJoijZ2Xi3AeL/2IEcaVpC04kuS89whEt74mYCLaAMa', 1);

INSERT INTO ticket (id_user, id_product, transaction_id, order_id, status) VALUES
('1', '1', '9227042407', '8055554606', 'reserved'),
('1', '1', '2802196119', '1096072751', 'reserved'),
('1', '2', '2802196119', '1096072751', 'reserved');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

