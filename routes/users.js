const router = require("express").Router()
const db = require("../database/db")
const bcrypt = require("bcryptjs")
const tokenProvider = require("../tools/tokenProvider")
const auth = require("../middleware/auth")
const { check, validationResult } = require("express-validator")

// MySQL2 query for finding user from database
const queries = {
  findUser: `SELECT id FROM user WHERE email LIKE ?;`
}

// Register validation uses express-validator read more about this here: https://express-validator.github.io/docs/
// This function configures the express-validator middleware, we are using this for example in @route POST api/users/register
// to validate user input
const registerValidations = () => {
  return [
    check("email", "Email is required")
      .not()
      .isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 8 or more characters"
    ).isLength({ min: 8 }),
    check("email").custom(async function(value) {
      try {
        const [results, fields] = await db.query(queries.findUser, value)
        // console.log(results)
        if (results.length > 0) {
          return Promise.reject("Email on jo käytössä") // In english: 'Email is already registered'
        }
      } catch (err) {}
    })
  ]
}

// @route POST api/users/register
// @desc Register a user
// @access Public
// TODO: Check if user already exists
router.post("/register", registerValidations(), async (req, res) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() })
  }

  const email = req.body.email
  const password = req.body.password

  console.log("User registering with email/pass: ", email, password)
  // This will hash and salt password by using bcrypt
  // Read more about this here: https://github.com/kelektiv/node.bcrypt.js#readme
  const passhash = await bcrypt.hash(password, 10)

  const user = {
    email: email,
    passhash: passhash,
    role: 1
  }

  let token
  let error

  try {
    const [results, fields] = await db.query("INSERT INTO user SET ?", user)

    const newUser = {
      id: results.insertId,
      email: user.email
    }
    // ("../tools/tokenProvider")
    // Creates a new user token
    token = tokenProvider.signToken(newUser)

    // console.log(results, fields)
  } catch (err) {
    console.log(err)
    error = err
  }

  if (error) {
    res.status(500).json({ message: "Server Error" })
  }

  res.json({ token: token, email: email, roleId: 1 })
})

// @route POST api/users/login
// @desc Login a user
// @access Public
router.post("/login", async (req, res) => {
  const email = req.body.email
  const password = req.body.password

  const query = "SELECT id, email, passhash, role FROM user WHERE email = ?"
  let newUser
  let roleId = 1

  try {
    // Retrieve user from the database (id, email, passhash, role)
    const [results, fields] = await db.query(query, email)
    // If no rows are returned, a non existing email was entered
    if (results.length === 0) {
      return res.status(400).json({ msg: "Invalid Credentials" })
    }
    // isMatch will be true, if password matches the hash, which is retrieved from the database
    const isMatch = await bcrypt.compare(password, results[0].passhash)
    // If password does not match the hash, return error
    if (!isMatch) {
      return res.status(400).json({ msg: "Invalid Credentials" })
    }
    // Construct a user object, this is used to sign the JWT
    newUser = {
      id: results[0].id,
      email: results[0].email
    }

    roleId = results[0].role
  } catch (err) {
    res.status(500).json({ msg: "Server Error" })
  }
  // Sign / create new token with the newUser as payload
  const token = tokenProvider.signToken(newUser)
  // console.log(user)
  res.json({ token: token, email: email, roleId: roleId })
})

// @route POST api/users/updaterole
// @desc Update a user role
// @access Private
router.post("/updaterole", auth, async (req, res) => {
  // Extract data from the request
  const data = {
    id: req.user.id,
    role: req.body.newRoleId
  }
  const query = "UPDATE user SET role = ? WHERE id = ?;"

  try {
    const [results, fields] = await db.query(query, [data.role, data.id])
    // No error, the query was successful
    return res.json({ msg: "Role changed" })
  } catch (err) {
    console.log(err)
    return res.status(500).json({ Error: "ServerError: Failed to change role" })
  }
})

module.exports = router
