const router = require("express").Router()
const db = require("../database/db")

// Test router

router.get("/", async (req, res) => {
  const query = "SELECT * FROM user;"

  try {
    const [rows, fields] = await db.execute(query)
    const result = rows[0]
    console.log(result.id, result.email, result.passhash, result.role)
  } catch (err) {
    console.log(err)
  }

  res.json("ok")
})

module.exports = router

// https://express-validator.github.io/docs/
