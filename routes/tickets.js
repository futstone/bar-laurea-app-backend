const router = require("express").Router()
const db = require("../database/db")
const bcrypt = require("bcryptjs")
const tokenProvider = require("../tools/tokenProvider")
const auth = require("../middleware/auth")
const ticketIsValid = require("../tools/ticketIsValid")

// MySQL2 queries
const queries = {
  // Add new ticket to the database
  add: "INSERT INTO ticket SET ?",
  // Retrieve user's all unused tickets
  retrieve: `SELECT user.role, ticket.id, ticket.order_id, ticket.transaction_id, product.name, price.amount
     FROM user 
     JOIN ticket ON user.id = ticket.id_user
     JOIN product ON ticket.id_product = product.id
     JOIN price ON product.id = price.id_product
     WHERE user.id = ? AND price.id_role = user.role AND ticket.status LIKE 'reserved';`,
  //  Retrieve user's specific ticket
  retrieveOne: `SELECT user.role, ticket.id, ticket.order_id, ticket.transaction_id, product.name, price.amount
     FROM user 
     JOIN ticket ON user.id = ticket.id_user
     JOIN product ON ticket.id_product = product.id
     JOIN price ON product.id = price.id_product
     WHERE ticket.id = ? AND user.id = ? AND price.id_role = user.role;`,
  //  Change ticket status from reserved to reclaimed
  useTicket: `UPDATE ticket SET status = 'reclaimed' WHERE ticket.order_id = ? AND ticket.status LIKE 'reserved';`
}

// @route POST api/tickets/add
// @desc Add a ticket
// @access Private
router.post("/add", auth, async (req, res) => {
  const ticket = {
    id_user: req.user.id,
    id_product: req.body.product_id,
    transaction_id: req.body.transaction_id,
    order_id: req.body.order_id,
    status: "reserved"
  }

  // If ticket is not valid / fields missing, return client error:
  if (!ticketIsValid(ticket)) {
    return res.status(400).send("Ticket is invalid")
  }

  try {
    // Results will be an array of objects, each object representing a single row from the resultset
    // Fields contains additional metadata of the query
    // Here we are making two different queries, first insert a ticket to the database
    // then use the inserted ticket's new id to retrieve it, so that we can return it back to the user right away in response
    const [results, fields] = await db.query(queries.add, ticket)
    const insertedTicketId = results.insertId
    const [results2, fields2] = await db.query(queries.retrieveOne, [
      insertedTicketId,
      req.user.id
    ])
    // results2 is an array and we want the first element, there is always just one element in the array in this result
    const newTicket = results2[0]
    console.log("NEW TICKET:", newTicket)
    // console.log(results, fields)
    // Return successful response with the new ticket as payload
    return res.json(newTicket)
  } catch (err) {
    console.log(err)
    return res.status(500).send("Server Error")
  }
})
// @route GET api/tickets/retrieve
// @desc Retrieve a ticket
// @access Private
router.get("/retrieve", auth, async (req, res) => {
  const userId = req.user.id

  try {
    const [results, fields] = await db.query(queries.retrieve, userId)
    return res.json(results)
    // console.log(results)
  } catch (err) {
    console.log(err)
    return res.status(500).send("Server Error")
  }
})
// @route POST api/tickets/use
// @desc Use a ticket
// @access Public
router.post("/use", async (req, res) => {
  // console.log(req.body)

  const receivedTicket = {
    order_id: req.body.id
  }

  // This is where we need to reclaim the reservation from MobilePays API
  // ...if MobilePay reclaim succeeds, we know the ticket is valid and now used
  // now we can flag the ticket as used in our own database (or simply remove the row completely)
  // lets change it to 'reclaimed' instead of removing for now, in a production version
  // we probably want to delete the row

  try {
    // Do MobilePay stuff
    // if this succeeds, the next try/catch is executed
  } catch (err) {
    // If MobilePay fails, send error, ticket was not valid and cannot be used, display red light or smth
    // res.status(500)...
  }

  // This try/catch structure will be executed if the above one executed succesfully, meaning that
  // the MobilePay API call was successful and the reservation is now reclaimed (customer has now finally paid the product)
  try {
    // Query the database with 'useTicket' query, this will set the ticket's status from 'reserved' to 'reclaimed'
    const [results, fields] = await db.query(
      queries.useTicket,
      receivedTicket.order_id
    )

    // Get the number of updated rows in database, this should always be exactly 1
    const numberOfUpdatedRows = results.affectedRows

    if (numberOfUpdatedRows === 0) {
      // Response with error
      return res.status(400).send("Invalid ticket.")
    }

    return res.json("Ticket was valid and is now reclaimed succesfully")
  } catch (err) {
    console.log(err)
    return res
      .status(500)
      .send(
        "The ticket was valid and is used, but there was an error in our own database"
      )
  }
})

module.exports = router
