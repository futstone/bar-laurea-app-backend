const router = require("express").Router()
const db = require("../database/db")
// Router for searching user's products
// MySQL2 documentation: https://www.npmjs.com/package/mysql2
// MySQL2 query for fetching products from database
const query = `
  SELECT product.id, product.name, price.amount 
  FROM product JOIN price
  ON product.id = price.id_product
  WHERE price.id_role = ?
`
// @route GET api/products/:id_role
// @desc Get products for certain role
// @access Public
router.get("/:id_role", async (req, res) => {
  console.log("Request products and prices for role: " + req.params.id_role)

  try {
    // Results will be an array of objects, each object representing a single row from the resultset
    // Fields contains additional metadata of the query, although we're not using it here for anything
    const [results, fields] = await db.execute(query, [req.params.id_role])
    res.json(results)
  } catch (err) {
    console.log("Error:", err)
  }
})

module.exports = router
