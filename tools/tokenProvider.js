// Purpose of this file is that it creates a new user token
const jwt = require("jsonwebtoken")
require("dotenv").config()

// user: { id, email }
const signToken = user => {
  // process.env.SECRET comes from .env file, it is used to sign the token
  // return the created token
  return jwt.sign(user, process.env.SECRET)
}

module.exports = { signToken }
