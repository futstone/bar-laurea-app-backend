// Global method for checking if the input is empty

const isEmpty = value =>
  value === undefined ||
  value === null ||
  (typeof value === 'object' && Object.keys(value).length === 0) || // Checking for empty object
  (typeof value === 'string' && value.trim().length === 0) // Checking for empty string

// Check for every key in ticket-object for emptyness, return false if one empty is found
module.exports = function(ticket) {
  let valid = true
  Object.keys(ticket).forEach(key => {
    if (isEmpty(ticket[key])) {
      valid = false
    }
  })
  return valid
}

//module.exports = isEmpty
